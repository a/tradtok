import os
import time
import math
import requests

branches = {"stable": "http://fw.ota.homesmart.ikea.net/feed/version_info.json",
            "test": "http://fw.test.ota.homesmart.ikea.net/"
                    "feed/version_info.json"}


def download_file(url, filename):
    # Based on http://masnun.com/2016/09/18/
    # python-using-the-requests-module-to-download-large-files-efficiently.html
    response = requests.get(url, stream=True)
    if response.status_code != 200:
        print(f"Failed download: {url}")
        return
    print(f"Downloading: {url}")
    handle = open(filename, "wb")
    for chunk in response.iter_content(chunk_size=512):
        if chunk:  # filter out keep-alive new chunks
            handle.write(chunk)


# Create required directories if they do not exist
for dirname in ["bin", "metadata"]:
    if not os.path.exists(dirname):
        os.makedirs(dirname)


print("TradTok by aveao. Licensed GPLv3, https://gitlab.com/ao/tradtok")
# I found out about the URLs from
# https://github.com/basilfx/TRADFRI-Hacking/pull/13
# so, HUGE shoutouts to oliv3r and basilfx


# Go through each branch
for branch in branches:
    print(f"Checking {branch} branch for updates")
    url = branches[branch]

    # Fetch metadata from tradfri servers
    metadata = requests.get(url)
    metadataj = metadata.json()
    save_metadata = False

    # Go through files in metadata
    for file in metadataj:
        dl_url = file["fw_binary_url"]
        dl_path = os.path.join("bin", dl_url.split("/")[-1])

        # Skip file if it is already downloaded
        if os.path.isfile(dl_path):
            continue

        # If file doesn't exist, mark metadata as worth saving
        save_metadata = True
        # and download the file
        download_file(dl_url, dl_path)

    # If we downloaded anything, then save the metadata as it might be useful
    if save_metadata:
        print(f"Saving {branch} metadata")
        metadata_path = os.path.join("metadata",
                                     f"{branch}-{math.floor(time.time())}.json")
        with open(metadata_path, "w") as f:
            f.write(metadata.text)

print("All done.")
