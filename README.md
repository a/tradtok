## Tradtok

Tool that downloads latest version information and version from TRADFRI servers, for archival, preservation and reverse engineering.

Yay, smart bulbs.

Requires python3.6+ and python-requests to run.

### Screenshots

![](https://acab.fyi/i/v1emsx4l.png)

![](https://acab.fyi/i/5b5ucqmk.png)

